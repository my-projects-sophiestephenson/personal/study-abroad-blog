<?php

#
# remove whitespace and limit the string to 64 characters to prevent
# people from sending something stupid
#
$max_email = 64;
$email = trim(substr(ltrim(@$_POST["email"]), 0, $max_email)) . "\n";

file_put_contents("_emails_", $email, FILE_APPEND);

header("location: index.html");